# STEGED: Hide files in Pictures
## Hides a file in a picture

### Usage

`Hide a file:    steged -w <inputimg> <outputimg> <tohide>
Read a file:    steged -r <hiddenin> (writes to STDOUT)
`
