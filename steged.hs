import Graphics.Image hiding (zipWith, map, sum)
import Data.Bits
import Data.Word
import Data.Char
import qualified Data.ByteString as BL
import Data.Binary.Get
import System.Environment

type Triple = [Bool]
data FlatImage = FlatImage (Int,Int) [Pixel RGB Double] deriving Show

mince :: Int -> [a] -> [[a]]
mince n [] = []
mince n a = take n a : mince n (drop n a)

hideBit' :: Double -> Bool -> Double
hideBit' v b = fromIntegral(hideBit(floor(255 * v)) b) / 255 where
    hideBit :: Int -> Bool -> Int
    hideBit v True  = 1 .|. v .&. 254
    hideBit v False = 0 .|. v .&. 254

extractBit :: Int -> Bool
extractBit v = v .&. 1 == 1
extractBit' :: Double -> Bool
extractBit' v = extractBit (floor (v * 255))

hideTripleInPixel :: [Bool] -> Pixel RGB Double -> Pixel RGB Double
hideTripleInPixel [x,y,z] (PixelRGB a b c)= PixelRGB (hideBit' a x) (hideBit' b y) (hideBit' c z)

extractTripleFromPixel :: Pixel RGB Double -> [Bool]
extractTripleFromPixel (PixelRGB a b c)= [extractBit' a, extractBit' b, extractBit' c]

toBitList :: Word8 -> [Bool]
toBitList x = fmap (testBit x) [0..(finiteBitSize x-1)]

fileToWord8 :: String -> IO [Word8]
fileToWord8 fs = do
    contents <- BL.readFile fs
    return $ BL.unpack contents

bytesToTriples :: [Word8] -> [[Bool]]
--bytesToTriples w = init triples ++ [last triples ++ replicate (3-l) False] where 
bytesToTriples w = init triples ++ [last triples ++ replicate (3-l) False] where 
    triples = (mince 3 . concatMap toBitList) w
    l = length $ last triples

hideInFlatImage :: FlatImage -> [Word8] -> FlatImage
hideInFlatImage (FlatImage s id ) d = FlatImage s (zipWith hideTripleInPixel (triples ++ repeat [False, False, False]) id) where
    triples = bytesToTriples d

extractFromFlatImage :: FlatImage -> [Word8]
extractFromFlatImage (FlatImage s d) = map bitListToWord8 $ mince 8 $ concatMap extractTripleFromPixel d

bitListToWord8 :: [Bool] -> Word8
bitListToWord8 l = sum $ zipWith f [0..7] d where
    d = map n l
    f a b = 2 ^ a * b
    n True = 1
    n False = 0

toFlatImage :: Image VU RGB Double -> FlatImage
toFlatImage i = FlatImage (dims i) (concat (toLists i))

flatImageToImg :: FlatImage -> Image VU RGB Double
flatImageToImg (FlatImage (x,y) d) = fromLists (mince y d)

main :: IO ()
main = do
    aargs <- getArgs
    let args = aargs ++ [" "]   --fuck'n dirty
    if "-i" == head args then do 
            img <- readImageRGB VU $ args!!1
            let a = dims img
            putStrLn ("Image can take " ++ show ( fromIntegral (uncurry (*) a) * 0.375) ++ " bytes of data...")
    else if "-w" == head args then do
        img <- readImageRGB VU $ args!!1
        a <- fileToWord8 $ args!!3
        let fimg = toFlatImage img
        let imgmsg = hideInFlatImage fimg a
        writeImage (args!!2) $ flatImageToImg imgmsg
    else if "-r" == head args then do
        img <- readImageRGB VU $ args!!1
        let fimg = toFlatImage img
        let message = extractFromFlatImage fimg
--        putStr (map (chr.fromEnum) message)
        BL.putStr $ BL.pack message
    else putStr "Usage:\n\
                \   Hide a file:    steged -w <inputimg> <outputimg> <tohide>\n\
                \   Read a file:    steged -r <hiddenin>\n"
